# Pídenos - UI

### **Challenge Flutter Español - The Dart Side**

## **Notas:**
Para obtener los packages, ejecute:
```
Flutter Pub Get
```
Para ejecutar el proyecto:
```
Start Debugging
```
#  Packages usados

- [Provider](https://pub.dev/packages/provider)
- [Bloc](https://pub.dev/packages/bloc)
- [Flutter Bloc](https://pub.dev/packages/flutter_bloc)
- [Equatable](https://pub.dev/packages/equatable)
- [Font Awesome Flutter](https://pub.dev/packages/font_awesome_flutter)


**CAPTURAS**

## Splash
<img src="./screenshots/splash.jpg" alt="Splash" width="200"/>

## Login
<img src="./screenshots/login1.jpg" alt="Login" width="200"/>
<img src="./screenshots/login2.jpg" alt="Login" width="200"/>

## Registro
<img src="./screenshots/registro1.jpg" alt="Registro" width="200"/>
<img src="./screenshots/registro2.jpg" alt="Registro" width="200"/>

## Selecciona Servicio
<img src="./screenshots/selecciona.jpg" alt="Selecciona" width="200"/>

## Mi Selección
<img src="./screenshots/servicio1.jpg" alt="MiSeleccion" width="200"/>
<img src="./screenshots/servicio2.jpg" alt="MiSeleccion" width="200"/>

## Animados

<img src="./screenshots/splash.gif" alt="MiSeleccion" width="200"/>
<img src="./screenshots/servicios.gif" alt="MiSeleccion" width="200"/>

