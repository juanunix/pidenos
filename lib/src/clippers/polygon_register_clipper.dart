import 'package:flutter/material.dart';

class PolygonRegisterClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) => Path()
    ..lineTo(size.width * .71, 0)
    ..arcToPoint(Offset(size.width * .75, size.height * .008),
        radius: Radius.circular(40))
    ..lineTo(size.width * .992, size.height * .26)
    ..arcToPoint(Offset(size.width, size.height * .3),
        radius: Radius.circular(40))
    ..lineTo(size.width, size.height * .68)
    ..arcToPoint(Offset(size.width * .992, size.height * .72),
        radius: Radius.circular(40))
    ..lineTo(size.width * .75, size.height * .992)
    ..arcToPoint(Offset(size.width * .71, size.height),
        radius: Radius.circular(40))
    ..lineTo(size.width * .29, size.height)
    ..arcToPoint(Offset(size.width * .26, size.height * .992),
        radius: Radius.circular(40))
    ..lineTo(size.width * .008, size.height * .72)
    ..arcToPoint(Offset(0, size.height * .68), radius: Radius.circular(40))
    ..lineTo(0, size.height * .3)
    ..arcToPoint(Offset(size.width * .008, size.height * .26),
        radius: Radius.circular(40))
    ..lineTo(size.width * .26, size.height * .008)
    ..arcToPoint(Offset(size.width * .3, 0), radius: Radius.circular(40))
    ..close();
  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => true;
}
