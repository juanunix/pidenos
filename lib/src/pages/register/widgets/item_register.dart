import 'package:flutter/material.dart';
import 'package:pidenos/src/pages/register/widgets/footer.dart';
import 'package:pidenos/src/pages/register/widgets/header.dart';
import 'package:pidenos/src/pages/register/widgets/polygom_form.dart';

class ItemRegister extends StatelessWidget {
  final String title;
  final String description;
  final String textBotton;
  final String text;
  final bool isCode;
  final Size size;
  final double top;
  final double left;
  final VoidCallback onPressed;
  ItemRegister({
    Key key,
    @required this.title,
    @required this.description,
    @required this.textBotton,
    @required this.text,
    @required this.isCode,
    @required this.size,
    @required this.top,
    @required this.left,
    @required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      behavior: HitTestBehavior.translucent,
      child: Container(
        width: size.width,
        height: size.height,
        child: Stack(
          children: <Widget>[
            Header(
              title: this.title,
              description: this.description,
            ),
            PolygomForm(
              top: this.top,
              left: this.left,
              tamanio: this.size,
              textBotton: this.textBotton,
              isCode: this.isCode,
              text: this.text,
              onPressed: this.onPressed,
            ),
            FooterRegister(),
          ],
        ),
      ),
    );
  }
}
