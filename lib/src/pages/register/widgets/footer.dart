import 'package:flutter/material.dart';
import 'package:pidenos/src/utils/responsive.dart';
import 'package:pidenos/src/widgets/text_widget.dart';

class FooterRegister extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _responsive = Responsive.of(context);
    return Positioned(
      bottom: _responsive.height * .005,
      child: Container(
        width: MediaQuery.of(context).size.width,
        child: TextWidget(
          text:
              'Estas a unos pasos de terminar \ntu registro nosotros te guiaremos.',
          color: Theme.of(context).accentColor,
          fontSize: _responsive.ip(1.95),
          letterSpacing: 1,
          maxLines: 2,
        ),
      ),
    );
  }
}
