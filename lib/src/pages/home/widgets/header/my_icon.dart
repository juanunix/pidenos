import 'package:flutter/material.dart';
import 'package:pidenos/src/widgets/text_widget.dart';

class MyIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(2),
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: Colors.white,
        shape: BoxShape.circle,
      ),
      child: TextWidget(
        padding: EdgeInsets.all(5),
        text: '?',
        color: Theme.of(context).primaryColor,
        fontSize: 20,
      ),
    );
  }
}
