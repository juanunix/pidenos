import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pidenos/src/blocs/home/home_bloc.dart';
import 'package:pidenos/src/blocs/home/home_state.dart';
import 'package:pidenos/src/pages/home/widgets/body/servicio_seleccionado/list_item.dart';
import 'package:pidenos/src/utils/responsive.dart';
import 'package:pidenos/src/widgets/text_widget.dart';

class ListaLugaresCercanos extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _responsive = Responsive.of(context);
    return Container(
      height: _responsive.ip(33),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        children: <Widget>[
          TextWidget(
            text: 'Cerca de ti',
            fontSize: _responsive.ip(2.2),
            padding: EdgeInsets.only(
              left: _responsive.wp(9),
              bottom: _responsive.hp(2),
            ),
            alignment: Alignment.centerLeft,
          ),
          Expanded(
            child: BlocBuilder<HomeBloc, HomeState>(
              builder: (_, state) => ListView.builder(
                shrinkWrap: true,
                physics: BouncingScrollPhysics(),
                scrollDirection: Axis.horizontal,
                itemCount: state.cercas.length,
                itemBuilder: (_, item) => ListItem(
                  item: item,
                  cerca: state.cercas[item],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
