import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pidenos/src/pages/login/widgets/form_login.dart';
import 'package:pidenos/src/pages/login/widgets/my_slideshow.dart';
import 'package:pidenos/src/utils/responsive.dart';
import 'package:pidenos/src/widgets/logo.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _responsive = Responsive.of(context);
    return Scaffold(
      body: SafeArea(
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          behavior: HitTestBehavior.translucent,
          child: SingleChildScrollView(
            child: Container(
              height: _responsive.height - _responsive.hp(2.85),
              child: Column(
                children: <Widget>[
                  Logo(porcentaje: .4, top: _responsive.hp(1.25)),
                  MySlishow(),
                  SizedBox(height: _responsive.hp(2.5)),
                  FormLogin(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
