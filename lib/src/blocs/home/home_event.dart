import 'package:pidenos/src/models/resenia_model.dart';
import 'package:pidenos/src/models/cerca_model.dart';

abstract class HomeEvent {}

class Selecting extends HomeEvent {}

class Recomendations extends HomeEvent {
  final List<CercaDeTiModel> cercas;
  final List<ReseniasModel> resenias;
  final String tittleSelection;
  Recomendations({this.cercas, this.resenias, this.tittleSelection});
}
