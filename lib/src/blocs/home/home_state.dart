import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart' show required;
import 'package:pidenos/src/models/resenia_model.dart';
import 'package:pidenos/src/models/cerca_model.dart';

enum HomeStatus { selecting, recomendations }

class HomeState extends Equatable {
  final HomeStatus status;
  final List<CercaDeTiModel> cercas;
  final List<ReseniasModel> resenias;
  final String tittleSelection;

  HomeState({
    @required this.status,
    @required this.cercas,
    @required this.resenias,
    @required this.tittleSelection,
  });

  static HomeState get initialState => HomeState(
        status: HomeStatus.selecting,
        cercas: [],
        resenias: [],
        tittleSelection: '',
      );

  HomeState copywith(
          {HomeStatus status,
          List<CercaDeTiModel> cercas,
          List<ReseniasModel> resenias,
          String tittleSelection}) =>
      HomeState(
        status: status ?? this.status,
        cercas: cercas ?? this.cercas,
        resenias: resenias ?? this.resenias,
        tittleSelection: tittleSelection ?? this.tittleSelection,
      );

  @override
  List<Object> get props => [status, resenias, cercas];
}
