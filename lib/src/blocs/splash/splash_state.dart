import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart' show required;

enum SplashStatus { loading, loaded }

class SplashState extends Equatable {
  final SplashStatus status;

  SplashState({@required this.status});

  static SplashState get initialState =>
      SplashState(status: SplashStatus.loading);

  SplashState copywith({SplashStatus status}) =>
      SplashState(status: status ?? this.status);

  @override
  List<Object> get props => [status];
}
