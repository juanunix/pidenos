import 'package:pidenos/src/models/resenia_model.dart';
import 'package:pidenos/src/models/cerca_model.dart';

class PanelModel {
  final double top;
  final double left;
  final double right;
  final List<CercaDeTiModel> cercas;
  final List<ReseniasModel> resenias;
  final String path;
  final String text;

  PanelModel({
    this.top,
    this.left,
    this.right,
    this.cercas,
    this.resenias,
    this.path,
    this.text,
  });
}
