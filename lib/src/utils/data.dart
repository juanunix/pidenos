import 'package:pidenos/src/models/register_model.dart';
import 'package:pidenos/src/models/resenia_model.dart';
import 'package:pidenos/src/models/panel_model.dart';
import 'package:pidenos/src/models/cerca_model.dart';

final List<String> items = [
  'assets/img/img-slide.png',
  'assets/img/img-slide2.png',
  'assets/img/img-slide3.png',
  'assets/img/img-slide4.png',
];

final List<String> codigo = ['8', '9', '5', '4'];

final List<String> numbers = ['+52', '442 3890 469'];

final List<PanelModel> panels = [
  PanelModel(top: 0, left: 135),
  PanelModel(top: 130, left: 135),
  PanelModel(
    top: 260,
    left: 135,
    cercas: restaurants,
    resenias: reseniasRestaurant,
    path: 'assets/img/comida.jpg',
    text: 'comida',
  ),
  PanelModel(top: 67, left: 20),
  PanelModel(
    top: 197,
    left: 20,
    cercas: zapaterias,
    resenias: reseniasZapaterias,
    path: 'assets/img/zapatos.jpg',
    text: 'zapatos',
  ),
  PanelModel(top: 327, left: 20),
  PanelModel(top: 67, right: 20),
  PanelModel(
    top: 197,
    right: 20,
    cercas: licorerias,
    resenias: reseniasLicoreria,
    path: 'assets/img/bebidas.jpg',
    text: 'licores',
  ),
  PanelModel(
      top: 327,
      right: 20,
      cercas: lavadoras,
      resenias: reseniasLavadoras,
      path: 'assets/img/carwash.jpg',
      text: 'lava auto'),
  PanelModel(top: 133, left: -95),
  PanelModel(top: 263, left: -95),
  PanelModel(top: 393, left: -95),
  PanelModel(top: 133, right: -95),
  PanelModel(top: 263, right: -95),
  PanelModel(top: 393, right: -95),
];

final List<CercaDeTiModel> restaurants = [
  CercaDeTiModel(
    lugar: 'El Tiburón',
    path: 'assets/img/restaurante.jpg',
    tipo: 'Restaurante',
  ),
  CercaDeTiModel(
    lugar: 'Santa Ana',
    path: 'assets/img/restaurante_2.jpg',
    tipo: 'Restaurante',
  ),
  CercaDeTiModel(
    lugar: 'El Tiburón',
    path: 'assets/img/restaurante.jpg',
    tipo: 'Restaurante',
  ),
];

final List<CercaDeTiModel> zapaterias = [
  CercaDeTiModel(
    lugar: 'El Flaco',
    path: 'assets/img/zapateria1.jpg',
    tipo: 'Zapateria',
  ),
  CercaDeTiModel(
    lugar: 'Calzado Paolita',
    path: 'assets/img/zapateria2.jpg',
    tipo: 'Zapateria',
  ),
  CercaDeTiModel(
    lugar: 'El Flaco',
    path: 'assets/img/zapateria1.jpg',
    tipo: 'Zapateria',
  ),
];

final List<CercaDeTiModel> licorerias = [
  CercaDeTiModel(
    lugar: 'Tía Santa Ana',
    path: 'assets/img/licoreria1.jpg',
    tipo: 'Licorería',
  ),
  CercaDeTiModel(
    lugar: 'Mundo Licor',
    path: 'assets/img/licoreria2.jpg',
    tipo: 'Licorería',
  ),
  CercaDeTiModel(
    lugar: 'Tía Santa Ana',
    path: 'assets/img/licoreria1.jpg',
    tipo: 'Licorería',
  ),
];

final List<CercaDeTiModel> lavadoras = [
  CercaDeTiModel(
    lugar: 'Lans',
    path: 'assets/img/lavadora1.jpg',
    tipo: 'Lavadora',
  ),
  CercaDeTiModel(
    lugar: 'Flash Cars',
    path: 'assets/img/lavadora2.jpg',
    tipo: 'Lavadora',
  ),
  CercaDeTiModel(
    lugar: 'Lans',
    path: 'assets/img/lavadora1.jpg',
    tipo: 'Lavadora',
  ),
];

final List<ReseniasModel> reseniasRestaurant = [
  ReseniasModel(
    path: 'assets/img/miguel.png',
    usuario: 'Miguel Hernandez',
    restaurante: 'Restaurante El Tiburón',
    resenia:
        'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd',
  ),
  ReseniasModel(
    path: 'assets/img/perla.png',
    usuario: 'Perla Reyes',
    restaurante: 'Restaurante Lodana',
    resenia:
        'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd',
  ),
];

final List<ReseniasModel> reseniasLicoreria = [
  ReseniasModel(
    path: 'assets/img/miguel.png',
    usuario: 'Miguel Hernandez',
    restaurante: 'Licorería Mundo Licor',
    resenia:
        'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd',
  ),
  ReseniasModel(
    path: 'assets/img/perla.png',
    usuario: 'Perla Reyes',
    restaurante: 'Licorería Tía Santa Ana',
    resenia:
        'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd',
  ),
];

final List<ReseniasModel> reseniasZapaterias = [
  ReseniasModel(
    path: 'assets/img/miguel.png',
    usuario: 'Miguel Hernandez',
    restaurante: 'Zapateria El Flaco',
    resenia:
        'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd',
  ),
  ReseniasModel(
    path: 'assets/img/perla.png',
    usuario: 'Perla Reyes',
    restaurante: 'Zapateria Calzado Paolita',
    resenia:
        'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd',
  ),
];

final List<ReseniasModel> reseniasLavadoras = [
  ReseniasModel(
    path: 'assets/img/miguel.png',
    usuario: 'Miguel Hernandez',
    restaurante: 'Lavadora Lans',
    resenia:
        'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd',
  ),
  ReseniasModel(
    path: 'assets/img/perla.png',
    usuario: 'Perla Reyes',
    restaurante: 'Lavadora Flash Cars',
    resenia:
        'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd',
  ),
];

final List<RegisterModel> registers = [
  RegisterModel(
    title: 'Requerimos tu numero \ntelefonico',
    description: 'Se te enviará un código de verificación',
    text: 'Solo será para verificar correctamente \ntus datos',
    textBotton: 'Siguiente',
    isCode: false,
    height: 1,
    width: 1,
    top: .22,
    left: 0,
  ),
  RegisterModel(
    title: 'Codigo de autorización',
    description: 'Se te envio un mensaje de texto',
    text: 'Introduce tu código',
    textBotton: 'Registrar',
    isCode: true,
    height: .55,
    width: .58,
    top: .2,
    left: .2,
  ),
  RegisterModel(
    title: 'Requerimos tu numero \ntelefonico',
    description: 'Se te enviará un código de verificación',
    text: 'Solo será para verificar correctamente \ntus datos',
    textBotton: 'Siguiente',
    isCode: false,
    height: 1,
    width: 1,
    top: .22,
    left: 0,
  ),
  RegisterModel(
    title: 'Codigo de autorización',
    description: 'Se te envio un mensaje de texto',
    text: 'Introduce tu código',
    textBotton: 'Registrar',
    isCode: true,
    height: .588,
    width: .55,
    top: .2,
    left: .2,
  ),
];
