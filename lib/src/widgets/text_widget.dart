import 'package:flutter/material.dart';

class TextWidget extends StatelessWidget {
  final String text;
  final double fontSize;
  final FontWeight fontWeight;
  final TextAlign textAlign;
  final double letterSpacing;
  final Color color;
  final String fontFamily;
  final EdgeInsetsGeometry padding;
  final AlignmentGeometry alignment;
  final int maxLines;
  final double width;

  TextWidget({
    Key key,
    @required this.text,
    this.fontSize = 16,
    this.fontWeight = FontWeight.bold,
    this.textAlign = TextAlign.center,
    this.letterSpacing = .5,
    this.color,
    this.fontFamily = 'sans_regular',
    this.padding = EdgeInsets.zero,
    this.alignment,
    this.maxLines = 1,
    this.width,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: this.alignment,
      padding: this.padding,
      width: this.width,
      child: Text(
        this.text,
        textAlign: this.textAlign,
        style: TextStyle(
          fontSize: this.fontSize,
          fontWeight: this.fontWeight,
          letterSpacing: this.letterSpacing,
          color: this.color ?? Theme.of(context).accentColor,
          fontFamily: this.fontFamily,
        ),
        maxLines: this.maxLines,
        overflow: TextOverflow.ellipsis,
      ),
    );
  }
}
