import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pidenos/src/utils/responsive.dart';

class InputTextLogin extends StatefulWidget {
  final String iconPath, placeHolder, initValue;
  final bool isOscure;
  final TextInputType textInputType;
  final String initialValue;
  final bool isVerification;
  final int maxLength;
  final double width;

  InputTextLogin({
    Key key,
    this.iconPath,
    this.placeHolder,
    this.isOscure = false,
    this.textInputType = TextInputType.text,
    this.initValue = '',
    this.initialValue,
    this.isVerification = false,
    this.maxLength,
    @required this.width,
  }) : super(key: key);

  @override
  _InputTextLoginState createState() => _InputTextLoginState();
}

class _InputTextLoginState extends State<InputTextLogin> {
  final _controller = TextEditingController();

  @override
  void initState() {
    if (widget.initValue != null) {
      _controller.text = widget.initValue;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final _responsive = Responsive.of(context);
    return Container(
      width: widget.width,
      child: CupertinoTextField(
        maxLength: widget.maxLength,
        placeholderStyle: TextStyle(
          color: Theme.of(context).accentColor.withOpacity(.5),
          fontFamily: 'sans_regular',
          fontSize: _responsive.ip(1.9),
          fontWeight: FontWeight.w400,
        ),
        style: TextStyle(
          color: Theme.of(context).accentColor,
          fontWeight: FontWeight.bold,
          fontSize: _responsive.ip(2.2),
        ),
        padding: EdgeInsets.only(left: (this.widget.iconPath != null) ? 20 : 5),
        prefix: (this.widget.iconPath != null)
            ? Container(
                width: 25,
                height: 14,
                margin: EdgeInsets.symmetric(
                  vertical: _responsive.hp(1),
                ),
                child: Image.asset(
                  this.widget.iconPath,
                  color: Theme.of(context).accentColor,
                ),
              )
            : null,
        obscureText: this.widget.isOscure,
        keyboardType: this.widget.textInputType,
        placeholder: this.widget.placeHolder,
        controller: _controller,
        decoration: BoxDecoration(
          border: (widget.isVerification)
              ? Border()
              : Border(
                  bottom: BorderSide(
                    width: 1.7,
                    color: Theme.of(context).accentColor,
                  ),
                ),
        ),
      ),
    );
  }
}
