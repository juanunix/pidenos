import 'package:flutter/material.dart';
import 'package:pidenos/src/pages/home/home_page.dart';
import 'package:pidenos/src/pages/login/login_page.dart';
import 'package:pidenos/src/pages/register/register_page.dart';
import 'package:pidenos/src/pages/splash/splash_page.dart';

Map<String, WidgetBuilder> getAplicationRoutes() {
  return <String, WidgetBuilder>{
    'splash': (BuildContext context) => SplashPage(),
    'login': (BuildContext context) => LoginPage(),
    'register': (BuildContext context) => RegisterPage(),
    'home': (BuildContext context) => HomePage(),
  };
}
