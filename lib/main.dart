import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pidenos/src/routes/routes.dart';

void main() {
  SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle.dark.copyWith(statusBarColor: Colors.transparent));
  return runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Pídenos',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Color(0xffFFCF60),
        accentColor: Color(0xff503D2E),
        fontFamily: 'sans',
      ),
      initialRoute: 'splash',
      routes: getAplicationRoutes(),
    );
  }
}
